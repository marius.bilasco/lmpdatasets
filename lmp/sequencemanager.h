/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef SEQUENCEMANAGER_H
#define SEQUENCEMANAGER_H

#include "lmp_common_header.h"
#include <sys/types.h>
#include <dirent.h>

class sequencemanager
{
public:
		sequencemanager();

		/**
		 * @brief load the landmark annotations for a sequence
		 * @param path (string) : path to the annotation file
		 * @param sequence (string) : name of the folder corresponding to the analysed folder
		 * @return vecteur de points (68pts par images), avec n images
		 */
		vector< vector<Point2f> > getLandmarksOneMilliseconde(string path, string sequence);

		/**
		 * @brief return a list containing the path to each pair of consecutive image
		 * 			in the sequence along with the optical flow
		 * @param path (string) : path to the subject folder
		 * @param file (string) : path to the sequence in the subject folder to be analysed
		 * @param method (string) : optical flow method to use
		 * @param nb (int) number of images in the folder
		 * @return return a list containing the path to each pair of consecutive image
		 * 			in the sequence along with the optical flow and the lmp filenames
		 */
	  vector< vector<string> > getImgsFlowsAndLMP(string path, string file, string method, int nb);

		/**
		 * @brief counts the number of images in a folder
		 * @param file (string) : path to the folder to analyze
		 * @return (int i) number of images
		 */
    int countImages(string file);

private:


    string _path, _file, _completPath, _method;
    vector< vector<int> > _annotation;
    vector<int> _class;
};

#endif // SEQUENCEMANAGER_H

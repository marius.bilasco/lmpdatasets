/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include <sys/stat.h>
#include "sequenceanalyser.h"

int SequenceAnalyser::_bins, SequenceAnalyser::_maxEmo;
int SequenceAnalyser::_L, SequenceAnalyser::_Lr, SequenceAnalyser::_D, SequenceAnalyser::_P,SequenceAnalyser::_E,SequenceAnalyser::_M,SequenceAnalyser::_V;
double SequenceAnalyser::_B, SequenceAnalyser::_Dr, SequenceAnalyser::_min, SequenceAnalyser::_max;
vector<FaceROI> SequenceAnalyser::_meshs;
vector< vector<double> > SequenceAnalyser::_motionVector, SequenceAnalyser::_GMD;
vector< vector<vector<double>> >SequenceAnalyser::_seqMotionVector ;
Mat SequenceAnalyser::flow, SequenceAnalyser::mask, SequenceAnalyser::frame;


SequenceAnalyser::SequenceAnalyser()
{

}

void SequenceAnalyser::analyze(int emotion, string sPath, string sSubject, string sSequence,  int L, double Dr, double B, int E, int M, int V, int P, int bin, string ofMethod)
{
		_emotion=emotion;
		_path = sPath;
		_subject = sSubject;
		_sequence = sSequence;
		_fullpath = sPath + sSubject + "/";

    _num_mesh = 25; //number of facial regions
    _ourAnalyse.initialize(bin, _num_mesh);

    _L = L;
    _B = B;
    _Dr = Dr;
    _E = E;
    _M = M;
    _V = V;
    _P = P;
    _bins = bin;

    if (strcmp(ofMethod.c_str(),"DIS")==0)
    	optFlowExtractor = optflow::createOptFlow_DIS();
    else if (strcmp(ofMethod.c_str(),"DeepFlow")==0)
    	optFlowExtractor = optflow::createOptFlow_DeepFlow();
    else if (strcmp(ofMethod.c_str(),"SparseToDense")==0)
    	optFlowExtractor = optflow::createOptFlow_SparseToDense();
    else { //default
    	ofMethod="farneback";
    	optFlowExtractor = optflow::createOptFlow_Farneback();
	}

  //loads the landmarks of the frames and the paths "_data" to each couple of successive frames and their OF and/or LMP files if already calculated
  sequencemanager _sequencemanager;
	_landmarks = _sequencemanager.getLandmarksOneMilliseconde(_fullpath, _sequence);
	_sequenceLength = _sequencemanager.countImages(_fullpath + _sequence);
	_data = _sequencemanager.getImgsFlowsAndLMP(_fullpath, _sequence, ofMethod, _sequenceLength);

	//we ignore sequences with less than 2 frames
	if (_sequenceLength<2)
		std::cerr<<"incomplete sequence : size = "<<_sequenceLength<<"\n";
	if (_sequenceLength<2)
		return ;

	runAnalysis();
}


void SequenceAnalyser::runAnalysis()
{

    Mat oldFrame, newFrame;
    vector<Point2f> oldFrameLands,newFrameLands;

    //number of OF (LMP) to be calculated
    int max = _sequenceLength - 1;

    Mat newF, oldF;
    _coherency = Mat::zeros(oldF.cols, oldF.rows, CV_8UC1);
    struct stat buffer;

    _GMD.clear();

    //initialize the GMD by zeros
    for(int i = 0; i < _num_mesh; i++)
    {
        vector<double> v(_bins,0);
        _GMD.push_back(v);
    }

    std::cerr<<"run analyses from 0 to "<<max<<" frames with emo type "<<_emotion<<"\n";

        for(int i = 0; i < max; i++)
        {
        	std::cerr<<i<<" ";
        	newF = imread(_data[i][1]);
        	//load the OF if already exist
        	if (stat (_data[i][2].c_str(), &buffer) == 0) {
        		 printf("loading OF from %s\n",_data[i][2].c_str());
				 flow = optflow::readOpticalFlow(_data[i][2]);
			 } else {
					//calculate the OF
					printf("computing OF from %s %s\n",_data[i][0].c_str(),_data[i][1].c_str());
					oldF=imread(_data[i][0]);

#ifdef SHOW_IMG
					imshow("old",oldF);
					imshow("new",newF);
					waitKey(1);
#endif

					cvtColor(oldF,oldF,CV_BGR2GRAY);
					cvtColor(newF,newF,CV_BGR2GRAY);
					optFlowExtractor->calc(oldF,newF,flow);
#ifdef	SAVE_OPT_FLOW
					//save the optical flow to the path in _data[i][2]
					optflow::writeOpticalFlow(_data[i][2],flow);
#endif
        	}

        //calculate the LMP between two frames
        extractMotion(newF,flow,_landmarks[i],i);

#ifdef SAVE_LMP
        //save the LMP to the path in _data[i][3]
				saveLMP(_motionVector, _data[i][3]);
#endif
				//cumulate the LMP in the GMD
				for (int j=0; j<_motionVector.size();j++)
					for (int k=0; k<_motionVector.at(j).size(); k++)
						_GMD.at(j).at(k)+=_motionVector.at(j).at(k);
      }

#ifdef SAVE_GMD
  		string seqGMD=_path + _subject + "/" + _sequence+".gmd";
  		saveLMP(_GMD,seqGMD);
#endif
}

void SequenceAnalyser::extractMotion(Mat inMat, Mat inFlow, vector<Point2f> landmarks,int indice_frame)
{
    _motionVector.clear();

    //initialiser le vecteur des LMP
    for(int i = 0; i < _num_mesh; i++)
    {
        vector<double> v(_bins,0);
        _motionVector.push_back(v);
    }

    frame = inMat;
    flow = inFlow;

    _ourSeparator.createROI(landmarks);

    _meshs = _ourSeparator.getMeshs();

    if(indice_frame==0)
    {
    	_L = _ourSeparator.getPatchSize(_L);
    	_D = _L - (_Dr * _L);
    }

    //construct the facial region mask
    mask = _ourSeparator.delimitateFace(flow);


    //creates threads from computing LMP per facial region
    thread *tt = new thread[_num_mesh - 1];

    for(int i = 0; i < _num_mesh - 1; ++i)
    {
        tt[i] = thread(extractRegionMotion,i);
    }

    extractRegionMotion(_num_mesh-1);

    for(int i = 0; i < _num_mesh - 1; ++i)
    {
        tt[i].join();
    }

    delete [] tt;

    //add the lmp of 2 frames to a vector containing all the LMP of the sequence
    _seqMotionVector.push_back(_motionVector);

}


void SequenceAnalyser::saveLMP(vector<vector<double>> motionVect, string filename)
{
		stringstream vaux;
		vaux<<_emotion;

		for(int i = 0; i < motionVect.size(); i++)
		{
				for(int j = 0; j < motionVect[0].size(); j++)
				{
					vaux << " ";
					vaux << motionVect[i][j];
				}
		}
		vaux << "\n";

		ofstream fichier(filename.c_str(), ios::out );
		if(fichier.good())
		{
				fichier << vaux.str();
				fichier.close();
		}
}

vector<float> SequenceAnalyser::getFlattenMotionVector()
{
    vector<float> simpleVector(_motionVector.size()*_motionVector[0].size(),0);
    int cpt = 0;

    for(int i = 0; i < _motionVector.size(); i++)
    {
        for(int j = 0; j < _motionVector[0].size(); j++)
        {
            simpleVector[cpt] = _motionVector[i][j];
            cpt++;
        }
    }

    return simpleVector;
}

vector<vector<double> > SequenceAnalyser::getMotionVector()
{
    return _motionVector;
}

vector<vector<double> > SequenceAnalyser::getGMD()
{
    return _GMD;
}

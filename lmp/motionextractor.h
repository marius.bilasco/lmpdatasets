
/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef MOTIONEXTRACTOR_H
#define MOTIONEXTRACTOR_H

#include "lmp_common_header.h"
#include "filterMotion/patch.h"
#include <thread>
#include <mutex>

class motionExtractor
{
public:

    motionExtractor();

    /**
     * @brief extract the mouvement from an epicentre
     * @param flux (Mat) : matrice of optical flow
     * @param mask (Mat) : mask to limit the region of the landmarks and put the rest to 0
     * @param bins (int) : histogram bins
     * @param center (Point2f) : position of the zpicentre in the image
     * @param iteration (int) : number of iteration when analysing around the epicentre
     * @return retourn a vector of double corresponding to the histogram of orientation
     */
    vector<double> ourExtractMotion(Mat flux, Mat mask, int bins, Point2f center, int iteration, double min);

    /**
     * @brief return the cumulated orientation histogram of all patchs in a region
    */
    vector<double> getOccurences();

    /**
     * @brief draw the optical flow of each patch in the region
     * @param picture (Mat) : image to draw the OF
     */
    void drawFlow(Mat &picture);

    /**
     * @brief return the coherence map
     * the map saves the patches already analysed
    */
    Mat getCoherencyMap();

    /**
     * @brief draw the lmp distribution of the region (color = orientation, dimension = magnitude)
     * @param picture (Mat) : image sur laquelle on dessine
     */
    void drawDistribution(Mat &picture);

    /**
     * @brief initialize the setting for motion extraction
     * @param lambda (int) : region size (percentage)
     * @param delta (double) : overlapping rate between 2 regions (between 0 and 1)
     * @param batta (double) : bhattacharrya coefficient to estimate the similarity between 2 orientation histograms (between 0 and 1)
     * @param ecart (int) : maximum intensity to consider a principal direction
     * @param mean (int) : maximum spreading to consider a principal direction
     * @param vari (int) : maximaum variance between 2 successive bins (variance = 5 equivalent to a slop between 0% and 95%)
     */
    void patchParam(int lambda, int delta, double batta, int ecart, int mean, int vari);


private:

  /**
   * @brief return the _bins orientation histogram of a matrix
   * @param values (Mat) : mouvement matrix
   * @return vector of double corresponding to the orientation histogram
   */
    vector<double> drawCurveOnHistogram(Mat values);

    /**
     * @brief Analyser the mouvement in the patch neighborhood (8 connexes)
     * @param previousPatch (Patch) : Patch parent corresponding to the previous iteration
     * @param flow (Mat) : matrix of optical flow
     */
    void diffusionMotion(Patch previousPatch, Mat flow);

    /**
     * @brief Analyser the mouvement between 2 neighbors patch
     * @param X (int) : X coordinate of the center of the patch to be analysed
     * @param Y (int) : Y coordinate of the center of the patch to be analysed
     * @param Flow (Mat) : matrix of the optical flow
     * @param previousPatch (Patch) : patch Parent ( previous iteration)
     */
    void patchNeighbor(int X, int Y, Mat Flow, Patch previousPatch);


    int _bins, _min;

    vector<Patch> _allPatch;

    vector<double> _distribution_cumul_patchs;

    Mat _coherencyMap;

    Point2f _center;

    int _lambda, _delta, _ecart, _mean, _vari;
    double _batta;

    int _minPatch;

};

#endif // MOTIONEXTRACTOR_H

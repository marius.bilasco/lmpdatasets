/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef FACEROI_H
#define FACEROI_H

#include "../lmp_common_header.h"

class FaceROI
{
public:
  FaceROI();

  Mat estimateMask(Mat inMat);
  vector<Point2f> getPoints();
  Point2f getMiddle();

  /**
   * @brief draw the regionb contour
   * @param inMat (Mat) : image for drawing
   */
  void drawLine(Mat &inMat);

  /**
   * @brief initialize the  region
   * @param vect (vector of point2f) : points of the region countour
   * @param min (double) : minimal magnitude considered within each face region
   */
  void init(vector<Point2f> vect, double min);

  /**
   * @brief return a vector encoding the region geometry using angles
   * @return a vector
   */
  vector<double> getGeometricInformation();

  /**
   * @brief return a vector encoding the region geometry using angles and distances
   * @return a vector
   */
  vector<double> getGeometricInformation2();

  /**
   * @brief return the minimal magnitude considered within each face region
   * @return
   */
   double getMin();


private:
  /**
   * @brief return the angle from 3 points A, B et C
   */
  double angleBetweenThreePoints(Point2f pointA, Point2f pointB, Point2f pointC);

  /**
   * @brief return the distance between 2 points A and B
  */
  double distance(Point2f A, Point2f B);


  vector<Point2f> _points;
  Point2f _middle;
  Mat _mask;
  double _min;
};

#endif // FACEROI_H

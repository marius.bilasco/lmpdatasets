
/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef OURROI_H
#define OURROI_H

#include "../lmp_common_header.h"
#include "faceseparator.h"

class ourROI : public FaceSeparator
{
public:
    ourROI();

    /**
     * @brief creation of the facial framework
     * @param landmarks (68 facial points )
     */
    void createROI(vector<Point2f> landmarks);

    /**
     * @brief create the facial mask from the externel landmarks (face contour)
     * @param inMat (Mat) : frame
     * @return the facial mask
     */
    Mat delimitateFace(Mat inMat);

    /**
     * @brief calculate the region dimension from the percentage lambda _L
     * @param L (int) : region size in percentage
     */
    int getPatchSize(int L);

protected:
    /**
     * @brief draw the landmarks of the face
     */
    void drawPoints(Mat inMat);

    /**
     * @brief estimate extra points from landmarks in order to create regions in face areas without landmarks as cheeks
     */
    void estimatePoints();

    /**
     * @brief estimation of the 25 regions from the landmarks and the extra points
     */
    void estimateROIs();

    vector<Point2f> _estimatedPoints;
};

#endif // OURROI_H

/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include "faceseparator.h"

FaceSeparator::FaceSeparator()
{

}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void FaceSeparator::createROI(vector<Point2f> landmarks)
{
    _landmarks = landmarks;
    _meshs.clear();
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
vector<FaceROI> FaceSeparator::getMeshs()
{
    return _meshs;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

Point2f FaceSeparator::getMiddle(Point2f p1, Point2f p2)
{
    Point2f m;

    m.x = (p1.x + p2.x) / 2;
    m.y = (p1.y + p2.y) / 2;

    return m;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
double FaceSeparator::distanceBetween2points(Point2f p1, Point2f p2)
{
    return sqrt(pow(p2.x - p1.x,2) + pow(p2.y - p1.y,2));
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
void FaceSeparator::drawMesh(Mat inMat)
{
    Mat img;
    inMat.copyTo(img);

    for(int i = 0; i < _meshs.size(); i++)
        _meshs[i].drawLine(img);

    imshow("meshs",img);
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void FaceSeparator::drawPoints(Mat inMat)
{
    imshow("points",inMat);
}

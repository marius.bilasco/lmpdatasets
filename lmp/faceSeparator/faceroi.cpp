/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include "faceroi.h"

FaceROI::FaceROI()
{
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void FaceROI::init(vector<Point2f> vect, double min)
{
    for(int i = 0; i < vect.size(); i++)
        _points.push_back(vect[i]);
    _min = min;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

/**
 * @brief calculate the mask corresponding to the region
 * @param inMat (Mat) : current frame
 * @return mask of the pixels inside the region
 */

Mat FaceROI::estimateMask(Mat inMat)
{
    vector<Point> tmp;

    for(int i = 0; i < _points.size(); i++)
        tmp.push_back((Point) _points[i]);

    Mat mask = Mat::zeros(inMat.rows, inMat.cols, CV_8UC1);
    fillConvexPoly(mask,tmp,Scalar(255,255,255));

    return mask;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

/**
 * @brief get the region contour
 */

vector<Point2f> FaceROI::getPoints()
{
    return _points;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

/**
 * @brief returns the region middle
 */

Point2f FaceROI::getMiddle()
{
    double X = 0, Y = 0;

    for(int i = 0; i < _points.size(); i++)
    {
        X += _points[i].x;
        Y += _points[i].y;
    }

    X = X / (double)_points.size();
    Y = Y / (double)_points.size();

    _middle.x = X;
    _middle.y = Y;

    return _middle;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

/**
 * @brief dessine les lignes de la region
 * @param inMat (Mat) : image sur laquelle dessiner
 */
void FaceROI::drawLine(Mat &inMat)
{
    for(int i = 0; i < _points.size()-1; i++)
        line(inMat,_points[i],_points[i+1],Scalar(0,0,255));

    line(inMat,_points[_points.size()-1],_points[0],Scalar(0,0,255));
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

double FaceROI::angleBetweenThreePoints(Point2f pointA, Point2f pointB, Point2f pointC)
{
    float a = pointB.x - pointA.x;
    float b = pointB.y - pointA.y;
    float c = pointB.x - pointC.x;
    float d = pointB.y - pointC.y;

    float atanA = atan2(a, b);
    float atanB = atan2(c, d);

    return (atanB - atanA)*180/CV_PI;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
double FaceROI::distance(Point2f p1, Point2f p2)
{
    return sqrt(pow(p2.x - p1.x,2) + pow(p2.y - p1.y,2));
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

vector<double> FaceROI::getGeometricInformation()
{
    vector<double> tmp;

    for(int i = 0; i < _points.size()-2; i++)
        tmp.push_back(angleBetweenThreePoints(_points[i],_points[i+1],_points[i+2]));

    tmp.push_back(angleBetweenThreePoints(_points[0],_points[_points.size()-1],_points[_points.size()-2]));
    tmp.push_back(angleBetweenThreePoints(_points[1],_points[0],_points[_points.size()-1]));

    for(int i = 0; i < _points.size()-1; i++)
        tmp.push_back(distance(_points[i],_points[i+1]));

    tmp.push_back(distance(_points[0],_points[_points.size()-1]));

    return tmp;
}

vector<double> FaceROI::getGeometricInformation2()
{
    vector<double> tmp;

    Point2f mid = getMiddle();

    //distance middle to corner
    for(int i = 0; i < _points.size(); i++)
        tmp.push_back(abs(sqrt(pow(mid.x-_points[i].x,2)+pow(mid.y-_points[i].y,2))));

    //angles between two corners
    for(int i = 0; i < _points.size()-1; i++)
        tmp.push_back(angleBetweenThreePoints(_points[i],mid,_points[i+1]));

    tmp.push_back(angleBetweenThreePoints(_points[_points.size()-1],mid,_points[0]));

    return tmp;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
double FaceROI::getMin()
{
    return _min;
}

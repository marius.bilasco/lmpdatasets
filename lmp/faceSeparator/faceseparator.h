/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef FACESEPARATOR_H
#define FACESEPARATOR_H

#include "../lmp_common_header.h"
#include "faceroi.h"

class FaceSeparator
{
public:
  FaceSeparator();

  /**
   * @brief return a vector of the regions
   * @return
   */
  vector<FaceROI> getMeshs();

  /**
   * @brief draw the facial regions
   */
  void drawMesh(Mat inMat);

  /**
   * @brief create the facial framework
   * @param landmarks
   */
  virtual void createROI(vector<Point2f> landmarks);

  /**
   * @brief show the inMat in a "points" window
   */
  virtual void drawPoints(Mat inMat);

protected:

  /**
   * @brief return the middle of 2 points A and B
   */
  Point2f getMiddle(Point2f A, Point2f B);

  /**
   * @brief return the distance between 2 points A and B
   */
  double distanceBetween2points(Point2f A, Point2f B);

  vector<FaceROI> _meshs;
  vector<Point2f> _landmarks;

};

#endif // FACESEPARATOR_H

/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef SEQUENCEANALYSER_H
#define SEQUENCEANALYSER_H

#include "lmp_common_header.h"

#include "faceSeparator/ourroi.h"
#include "faceSeparator/faceroi.h"
#include "motionextractor.h"
#include "faceAnalyser/ouranalyser.h"
#include "sequencemanager.h"

class SequenceAnalyser
{
public:
    SequenceAnalyser();


    /**
    * @brief prepares the execution of GMD extraction from a sequence sSequence
    of a subject sSubject in a dataset located at sPath, using the specified
    parameters L, Dr, B, E, M, V, P using bin bins and the optical method
    ofMethod. the emotion parameter indicates the sequence emotion label.

    * @param L (int) : % size of facial patches to analyse
    * @param Dr (double) : % of overlapping between two patches
    * @param B (double) : bhattacharrya coefficient threshold for estimating the similarity of two direction histograms
    * @param E (int) : minimum intesity to consider a main direction
    * @param M (int) : maximum spreading to consider a principal direction
    * @param V (int) : maximaum variance between 2 successive bins (variance = 5 equivalent to a slop between 0% and 95%)
    * @param P (int) : number of recursive propagation for analysing neighbooring patches
    */
    void analyze(int emotion, string sPath, string sSubject, string sSequence, int L, double Dr, double B, int E, int M, int V, int P, int bin, string ofMethod);

    /**
     * @brief execute the lmp extractor code in several threads, one per region
     * @param index_region (int) : number of the region in the facial segmentation model
     */
    static void extractRegionMotion(int index_region)
    {
        motionExtractor _localMotion;

        //draw the region contour
        _meshs[index_region].drawLine(frame);
        //initialize the patch parameters
        _localMotion.patchParam(_L, _D, _B, _E, _M, _V);

        //extract the motion of the region "index_region"
        vector<double> motion = _localMotion.ourExtractMotion(flow, mask, _bins, _meshs[index_region].getMiddle(), _P, _meshs[index_region].getMin());

        //draw the visual distribution on the face (attribute frame)
        _localMotion.drawDistribution(frame);

        //draw the OF on the face (attribute frame)
        _localMotion.drawFlow(frame);
        _motionVector[index_region] = motion;
    }

    /**
     * @brief  calculate the LMP from inFlow considering the landmarks already provided
     */
    void extractMotion(Mat inMat, Mat flow, vector<Point2f> landmarks,int indice_frame);

    /**
     * @brief calculate the lmp between all the successive frames and cumulate them obtaining the final the GMD
     */
    void runAnalysis();

    /**
     * @brief  flattens a per region motion vector into a global motion vector
     */
    vector<float> getFlattenMotionVector();

    /**
     * @brief  save the provided motion vector (LMP or GMD) to the given filename
     */
    void saveLMP(vector<vector<double>> cumul, string filename);

    Mat getFrame();
    vector<vector<double> > getMotionVector();
    vector< vector<double> > getGMD();

private:
    vector< vector<Point2f> > _landmarks;
    vector< vector<string> > _data;

    Ptr<DenseOpticalFlow> optFlowExtractor;

    string _path, _subject, _sequence, _fullpath, _emo_cfg, _lmp_dir_suffix;
    string _nameSave;

    int _emotion;
    int _subjectNumber,_sequenceLength;

    ourROI _ourSeparator;
    ourAnalyser _ourAnalyse;

    Mat _coherency;

    int _num_mesh;

    static int _bins,_maxEmo;
    static int _L, _Lr, _D, _P, _E, _M, _V;
    static double _B, _Dr, _min, _max;
    static vector<FaceROI> _meshs;
    static vector< vector<double> > _motionVector;
    static vector< vector<vector<double>> > _seqMotionVector;
    static Mat flow, mask, frame;
    static vector< vector<double> > _GMD;


};

#endif // SEQUENCEANALYSER_H

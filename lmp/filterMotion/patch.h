/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef PATCH_H
#define PATCH_H

#include <iostream>
#include <vector>
#include <string>

#include <cmath>
#include <ctime>
#include <cstdio>
#include <cassert>
#include <cstdarg>
#include <cstdlib>
#include <fstream>

#include <opencv2/opencv.hpp>
#include <opencv2/optflow.hpp>

using namespace std;
using namespace cv;

class Patch
{
public:

  Patch();
  /**
   * @brief initialize the patch settings
   * @param x (int) : X coordinate of the centre of the patch on the image
   * @param y (int) : Y coordinate of the centre of the patch on the image
   * @param flow (Mat) : matrix of the optical flow
   * @param bins (int) : number of the histogram bins
   * @param iteration (int) : iteration counter for the motion diffusion
   * @param lambda (int) : dimension of the patch
   * @param delta (int) : overlapping rate between 2 regions (between 0 and 1)
   * @param batta (double) : bhattacharrya coefficient to estimate the similarity between 2 orientation histograms (between 0 and 1)
   * @param ecart (int) : maximum intensity to consider a principal direction
   * @param mean (int) : maximum spreading to consider a principal direction
   * @param vari (int) : maximaum variance between 2 successive bins (variance = 5 equivalent to a slop between 0% and 95%)
   */
  void initialize(int x, int y, Mat flow, int bins, int iteration, double min, int lambda, int delta, double batta, int ecart, int mean, int vari);

  /**
   * @brief allow to know if the local distribution is consistent for spread the motion
   * at least one principal direction has to be found in the filtered histogram
   * @return True if the local motion is coherent
   */
  bool isCoherent();

  /**
   * @brief verify the similarity between two patchs using the Bhattacharrya distance between their distributions
   * @param distribution (vector double) : distribution d'un patch voisin
   * @return True if the distance <= _batta
   */
  bool isCoherentWith(vector<double> distribution);

  /**
   * @brief return the current iteration counter value
   * @return
   */
  int getIteration();
  /**
   * @brief set the current iteration counter value
   */
  void setIteration(int ite);

  /**
   * @brief get the X coordinate of the patch center
   * @return
   */
  int getX();

  /**
   * @brief get the Y coordinate of the patch center
   * @return
   */
  int getY();

  /**
   * @brief get the patch dimension
   * @return
   */
  int getDims();


  /**
   * @brief return the jump between 2 patches
   * @return
   */
  int getJump();

  /**
   * @brief return the mouvement orientation distribution of the patch
   * @return
   */
  vector<double> getDistribution();

  /**
   * @brief return the mouvement orientation distribution of the patch
   * @return
   */
  vector<double> getFilteredDistribution();

  /**
   * @brief a recurssive function that count the number of successive bins >= variance. It stops when reaching the maximum spreading value _mean
   */
  void divergence(vector<int>* tmpFocus, int focus, vector<int> norma, int pas, int variance);

  /**
   * @brief returns the _bins orientation histogram
   * @param values (Mat) : mouvement matrix
   */
  vector<double> drawCurveOnHistogram(Mat values);

  /**
   * @brief calculate the mouvement orientation occurence according to 3 layers 0-33-66-100
   * @param allMagnitudeHistogram : distribution of the mouvement orientation for all magnitude intervals
   */
  void computeCoherentMotion(vector< vector<double> > fiveHistograme);

  /**
   * @brief draw the mouvement distribution in the patch
   * @param picture (Mat) : image for draw
   * @param bin (int) : number of the histogram bins
   * @return
   */
  void drawPatch(Mat &picture, int bin);

private:
  /**
   * @brief calculate the local distribution inside the patch
   * @param flux (Mat) : matrix of optical flow
   */
  void computeLocalDistribution(Mat flux);

  /**
   * @brief normalize the orientation histogram of a patch
   */
  void normalizeDistribution();

  int _dims = 20;//15
  int _jump = 15;//10

  double _batta;
  int _ecart, _mean, _vari;

  double _min;
  int _iterationCount;

  int _X, _Y, _bins;
  Mat _flow;

  vector<double> _distribution, _filteredDistribution;
  vector<int> _normaDistribution;
};

#endif // PATCH_H


/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#include "motionextractor.h"

motionExtractor::motionExtractor()
{
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

vector<double> motionExtractor::drawCurveOnHistogram(Mat values)
{
    vector<double> distribution;

    /// Establish the number of bins
    int histSize = _bins;
    float range[] = {0 , 360 } ;

    const float* histRange = { range };

    Mat b_hist;

    /// Compute the histograms:
    calcHist( &values, 1, 0, Mat(), b_hist, 1, &histSize, &histRange);

    /// Draw for each channel
    for( int i = 0; i < histSize; i++ )
    {
        distribution.push_back(b_hist.at<float>(i));
    }

    return distribution;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
void motionExtractor::patchParam(int lambda, int delta, double batta, int ecart, int mean, int vari)
{
    _lambda = lambda;
    _delta = delta;
    _batta = batta;
    _ecart = ecart;
    _mean = mean;
    _vari = vari;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

vector<double> motionExtractor::ourExtractMotion(Mat flux, Mat mask, int bins, Point2f center, int iteration, double min)
{
    _bins = bins;
    _allPatch.clear();
    _min = min;
    _center = center;

    _minPatch = 3;

    _coherencyMap = Mat::zeros(flux.rows,flux.cols,CV_8UC1);
    mask.copyTo(_coherencyMap);

    Patch epicentre;
    epicentre.initialize(center.x, center.y, flux, bins, iteration, _min, _lambda, _delta, _batta, _ecart, _mean, _vari);

    if(epicentre.isCoherent())
    {
        _allPatch.push_back(epicentre);
        _coherencyMap.at<uchar>(epicentre.getY(),epicentre.getX()) = 255;
        rectangle(_coherencyMap,Rect(Point(epicentre.getX()-1,epicentre.getY()-1),Point(epicentre.getX()+1,epicentre.getY()+1)),Scalar(255),-1);
        diffusionMotion(epicentre, flux);
    }

    vector<double> cumul(bins,0);

    if(_allPatch.size() > _minPatch)
    {
        for(int i = 0; i < _allPatch.size(); i++)
        {
            vector<double> distrib = _allPatch[i].getFilteredDistribution();
            for(int j = 0; j < cumul.size(); j++)
            {
                cumul[j] += distrib[j];
            }
        }
    }

    _distribution_cumul_patchs = cumul;

    return cumul;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
void motionExtractor::diffusionMotion(Patch previousPatch, Mat flow)
{
    if(previousPatch.getIteration() > 0)
    {
        previousPatch.setIteration(previousPatch.getIteration()-1);

        patchNeighbor(previousPatch.getX()+previousPatch.getJump(), previousPatch.getY(), flow, previousPatch);
        patchNeighbor(previousPatch.getX()+previousPatch.getJump(), previousPatch.getY()-previousPatch.getJump(), flow, previousPatch);
        patchNeighbor(previousPatch.getX(), previousPatch.getY()-previousPatch.getJump(), flow, previousPatch);
        patchNeighbor(previousPatch.getX()-previousPatch.getJump(), previousPatch.getY()-previousPatch.getJump(), flow, previousPatch);
        patchNeighbor(previousPatch.getX()-previousPatch.getJump(), previousPatch.getY(), flow, previousPatch);
        patchNeighbor(previousPatch.getX()-previousPatch.getJump(), previousPatch.getY()+previousPatch.getJump(), flow, previousPatch);
        patchNeighbor(previousPatch.getX(), previousPatch.getY()+previousPatch.getJump(), flow, previousPatch);
        patchNeighbor(previousPatch.getX()+previousPatch.getJump(), previousPatch.getY()+previousPatch.getJump(), flow, previousPatch);
    }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void motionExtractor::patchNeighbor(int X, int Y, Mat Flow, Patch previousPatch)
{
    if(_coherencyMap.at<uchar>(Y,X) == 0)
    {
        if(X < Flow.cols - (previousPatch.getDims()*2) && X > previousPatch.getDims()*2)
        {
            if(Y > previousPatch.getDims()*2 && Y < Flow.rows - (previousPatch.getDims()*2))
            {
                Patch childPatch;
                childPatch.initialize(X,Y,Flow, _bins,previousPatch.getIteration(), _min, _lambda, _delta, _batta, _ecart, _mean, _vari);

                if(childPatch.isCoherentWith(previousPatch.getDistribution()))
                {
                    _coherencyMap.at<uchar>(Y,X) = 255;
                    rectangle(_coherencyMap,Rect(Point(X-1,Y-1),Point(X+1,Y+1)),Scalar(255),-1);
                    if(childPatch.isCoherent())
                    {
                        diffusionMotion(childPatch, Flow);
                    }
                    _allPatch.push_back(childPatch);
                }
            }
        }
    }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
vector<double> motionExtractor::getOccurences()
{
    return _distribution_cumul_patchs;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
void motionExtractor::drawFlow(Mat &picture)
{
    if(_allPatch.size() > _minPatch)
    {
        for(int i = 0; i < _allPatch.size(); i++)
        {
            _allPatch[i].drawPatch(picture,_bins);
        }
    }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
Mat motionExtractor::getCoherencyMap()
{
    return _coherencyMap;
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */

void motionExtractor::drawDistribution(Mat &picture)
{
    if(_allPatch.size() > _minPatch)
    {
        Point2f p = _center;

        for(int i = 0; i < _distribution_cumul_patchs.size(); i++)
        {
            double angle = (i+1)*(360/_bins);

            double drift = 0.0005 * _distribution_cumul_patchs[i];

            double x = drift*cos(angle*CV_PI/180.0);
            double y = drift*sin(angle*CV_PI/180.0);

            Point2f q = Point2f(p.x + x, p.y - y);

            if(q.x < picture.cols-1 && q.y < picture.rows-1)
            {
                double val,R,G,B;

                if(angle >= 270) //B-G
                {
                    val = abs(angle-270);
                    G = 3 * val;
                    B = 255 - G;
                    R = 0;
                }
                else if(angle >= 180) //R-B
                {
                    val = abs(angle-180);
                    G = 0;
                    B = 3 * val;
                    R = 255-B;
                }
                else if(angle >= 90) //J-R
                {
                    val = abs(angle-90);
                    G = 255 - (3 * val);
                    B = 0;
                    R = 255;
                }
                else //G-J
                {
                    val = angle;
                    G = 255;
                    B = 0;
                    R = 3 * val;
                }

                line(picture, p, q, Scalar(B, G, R ),2);
            }
        }
    }
}

/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef FACEANALYSER_H
#define FACEANALYSER_H

#include "../lmp_common_header.h"
#include "../faceSeparator/faceroi.h"

class faceAnalyser
{
public:
    faceAnalyser();

    virtual void createVector(vector< vector<double> > facialMotion, Mat flow, vector<FaceROI> meshs, Mat frame);

    vector< vector<double> > getVector();

protected:
    vector<double> _meanVectorFrame;
    vector< vector<double> > _meanVectorSequence;
};

#endif // FACEANALYSER_H


/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/

#ifndef GLOBALHEADER_H
#define GLOBALHEADER_H

#define SAVE_OPT_FLOW
#define SAVE_LMP
//#define SHOW_IMG

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <math.h>
#include <opencv2/optflow.hpp>
#include <cstdlib>
#include <algorithm>
#include <opencv2/xfeatures2d.hpp>
#include <cmath>

using namespace std;
using namespace cv;

#endif // GLOBALHEADER_H

/*
lmp_taffc
Copyright (C) 2019  Université de Lille, Laboratoire CRIStAL, UMR 9189

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>
*/
#include "lmp/sequenceanalyser.h"

void writeGMDForSVM(int emotion, vector<vector<double>> motionVect, ofstream &fichier);

int main(int argc, char *argv[])
{

  vector<string> subjectFolder, expressionFolders;

  char path_str[2000];
  sprintf(path_str,"%s/",argv[9]);
  string path=path_str;

  char emo_cfg[200];
  strcpy(emo_cfg,argv[10]);

  int L,E,M,V,P,bin;
  float Dr,B;

  L=atoi(argv[1]);
  Dr=atof(argv[2]);
  B=atof(argv[3]);
  E=atoi(argv[4]);
  M=atoi(argv[5]);
  V=atoi(argv[6]);
  P=atoi(argv[7]);
  bin=atoi(argv[8]);

  //Optical flow method
  char ofMethod[20];
  if (argc>11)
  	strcpy(ofMethod,argv[11]);
  else
  	strcpy(ofMethod,"farneback");

  string attribute;

  //path to the data configuration file
  string csvFile=path+emo_cfg;

	ifstream myStream(csvFile.c_str());

	printf("loading data configuration file from %s \n",csvFile.c_str());

	getline(myStream, attribute, '\n');
	string subject, sequence;
	int emotion;

	SequenceAnalyser _seqAnalyser;
	char _nameSaveCStr[2000];

#ifdef SAVE_LMP
	char _nameSave[2000];
	time_t now = time(0);
	sprintf(_nameSave,"GMD_%s_L%d_Dr%.2f_B%.2f_E%d_M%d_V%d_P%d_Bin%d_%ld.txt",emo_cfg,L,Dr,B,E,M,V,P,bin,now);
	ofstream _file(_nameSave, ios::out );
#endif
	//Read the dataset configuration file
	while(!myStream.eof())
	{
		getline(myStream, subject, ';');
		if (subject.size()==0)
			break;
		printf("%s - ",subject.c_str());

		getline(myStream, sequence, ';');
		printf("%s - ",sequence.c_str());

		getline(myStream, attribute, '\n');
		emotion = atoi(attribute.c_str());
		printf("%d\n",emotion);

		_seqAnalyser.analyze(emotion,path,subject,sequence,L,Dr,B,E,M,V,P,bin,ofMethod);

#ifdef SAVE_LMP
		writeGMDForSVM(emotion, _seqAnalyser.getGMD(), _file);
#endif
  }

#ifdef SAVE_LMP
  _file.close();
#endif

  return 0;
}

void writeGMDForSVM(int emotion, vector<vector<double>> GMD, ofstream &fichier)
{
		stringstream vaux;
		vaux<<emotion;

		for(int i = 0; i < GMD.size(); i++)
		{
				for(int j = 0; j < GMD[0].size(); j++)
				{
					vaux << " " ;
					vaux << ((i*GMD[0].size())+j+1);
					vaux << ":";
					vaux << GMD[i][j];
				}
		}
		vaux << "\n";
		fichier << vaux.str();
}
